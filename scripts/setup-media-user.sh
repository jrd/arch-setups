#!/bin/bash

cat > $HOME/.bash_profile << 'EOF'

# default applications
export EDITOR=nvim

# XKB
export XKB_DEFAULT_LAYOUT=de

# firefox
export MOZ_ENABLE_WAYLAND=1
export MOZ_DISABLE_RDD_SANDBOX=1

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]
then
    pipewire &>/dev/null &
    pipewire-pulse &>/dev/null &
    pipewire-media-session &>/dev/null &
    exec ~/main_menu.sh
fi
EOF


# SETUP MAIN MENU ##############################################################
cat > $HOME/main_menu.sh << 'EOF'
#!/bin/bash
#Start this script on startup (eg. by .bash_profile entry)

HEIGHT=11
WIDTH=45
CHOICE_HEIGHT=4
BACKTITLE="Minimal Media Station v1"
MENU=""

function main_menu
{
title=" Main Menu "
options=(1 "Start Internet Browser (chromium)"
	 2 "Start Media-Center (kodi)"
	 3 "Start File Browser (ranger)"
         4 "Settings"
 	 5 "Poweroff")

choice=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$title" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${options[@]}" \
                2>&1 >/dev/tty)

clear
case $choice in
        1)
            cage -d chromium
            ;;
        2)
            cage -d kodi-standalone
            ;;
        3)
            ranger
            ;;
        4)
	    settings_menu
            ;;
        5)
	    sudo poweroff
            ;;
        *)
	    fish
            ;;
esac
}

function settings_menu
{
title="Settings"
options=(1 "Update System"
	 2 "Audio Control")

choice=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$title" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${options[@]}" \
                2>&1 >/dev/tty)

clear
case $choice in
        1)
            sudo pacman -Syu
	    read -p "Press any button to return to the main menu."
            ;;
        2)
	    echo "Starting pavucontrol. Typ Ctl-q to return to main menu!"
	    sleep 2
            cage -d pavucontrol
            ;;
        *)
            break
            ;;
esac
}

while true
do
    main_menu
done
EOF

cat > $HOME/.dialogrc << EOF
aspect = 0
separate_widget = ""
tab_len = 0
visit_items = OFF
use_shadow = OFF
use_colors = ON
screen_color = (WHITE,DEFAULT,OFF)
shadow_color = (WHITE,BLACK,OFF)
dialog_color = (WHITE,BLACK,OFF)
title_color = (GREEN,BLACK,OFF)
border_color = (WHITE,BLACK,OFF)
border2_color = border_color
button_active_color = (BLACK,YELLOW,OFF)
button_inactive_color = (WHITE,BLACK,OFF)
button_key_active_color = (BLACK,GREEN,OFF)
button_key_inactive_color = (RED,BLACK,OFF)
button_label_active_color = (BLACK,YELLOW,OFF)
button_label_inactive_color = (WHITE,BLACK,OFF)
inputbox_color = (WHITE,BLACK,OFF)
inputbox_border_color = (BLACK,BLACK,OFF)
inputbox_border2_color = (BLACK,BLACK,OFF)
searchbox_color = (WHITE,BLACK,OFF)
searchbox_title_color = (GREEN,BLACK,OFF)
searchbox_border_color = (WHITE,BLACK,OFF)
searchbox_border2_color = (WHITE,BLACK,OFF)
position_indicator_color = (GREEN,BLACK,OFF)
menubox_color = (BLACK,BLACK,OFF)
menubox_border_color = (BLACK,BLACK,OFF)
menubox_border2_color = (BLACK,BLACK,OFF)
item_color = (WHITE,BLACK,OFF)
item_selected_color = (BLACK,GREEN,OFF)
tag_color = (BLUE,BLACK,OFF)
tag_selected_color = (BLACK,GREEN,OFF)
tag_key_color = (YELLOW,BLACK,OFF)
tag_key_selected_color = (BLACK,GREEN,OFF)
check_color = (WHITE,BLACK,OFF)
check_selected_color = (BLACK,GREEN,OFF)
uarrow_color = (GREEN,BLACK,OFF)
darrow_color = (GREEN,BLACK,OFF)
itemhelp_color = (BLACK,WHITE,OFF)
form_active_text_color = (BLACK,BLUE,OFF)
form_text_color = (WHITE,BLACK,OFF)
form_item_readonly_color = (BLACK,WHITE,OFF)
EOF

cat > $HOME/.config/chromium-flags.conf << EOF
# wayland support
--ozone-platform-hint=auto

# force hw acceleration
--ignore-gpu-blocklist
--enable-gpu-rasterization
--enable-zero-copy

#--force-device-scale-factor=3
EOF
