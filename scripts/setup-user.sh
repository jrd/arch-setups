#!/bin/bash

# DEFAULT PARAMETERS ###########################################################
USER_NAME=""
USER_PASSWORD=""
INIT_GROUP="users"
ADDITIONAL_GROUPS="audio,video,power,wheel"
SHELL="/bin/bash"
################################################################################

# ADD USER #####################################################################
echo -e "\nADDING USER ...\n"

useradd -g $INIT_GROUP -G $ADDITIONAL_GROUPS -s $SHELL $USER_NAME
echo -e "$USER_PASSWORD\n$USER_PASSWORD" | passwd $USER_NAME 

mv /home/will_be_replaced_by_username /home/$USER_NAME
chown -R $USER_NAME:$INIT_GROUP /home/$USER_NAME
################################################################################

# AUTOLOGIN TO USER ############################################################
echo -e "\nADDING AUTOLOGIN FOR $USER_NAME ...\n"
autologin_dir="/etc/systemd/system/getty@tty1.service.d"
mkdir -p $autologin_dir
cat > $autologin_dir/autologin.conf << EOF
[Service]
Type=simple
ExecStart=
ExecStart=-/sbin/agetty --autologin $USER_NAME --noclear %I 38400 linux
EOF
################################################################################

echo -e "\nXXX RUNNING USER ACTION XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n"
su -c ./home/$USER_NAME/userAction.sh $USER_NAME
rm /home/$USER_NAME/userAction.sh
echo -e "\nXXX USER ACTION FINISHED XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n"
