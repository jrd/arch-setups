#!/bin/sh
cat $0 | tail -n +4 | sed -n '/```/,/```/p' | sed 's/```//g' | sed '/^$/d' | sh; exit #``
(execute this file to pipe every line of code to your standard command interpreter)

# Setup Personal User Environment

## Git

### Make git directories:
```
mkdir -p $HOME/.config/git
mkdir -p $HOME/.local/share/git
```

### Establish git files:
```
touch $HOME/.config/git/gonfig
touch $HOME/.local/share/git/known_hosts
chmod 644 $HOME/.local/share/git/known_hosts
```

### Set user name and email:
```
git config --global user.name "JRD"
git config --global user.email "745-j-r.d@users.noreply.git.tu-berlin.de"
```

### Set ssh command
In order to use the key and the known_host file dedicated to the git server,
the ssh command used by the git client must be modified.
```
git config --global core.sshCommand "ssh -i /media/key/git/git_rsa -F /dev/null -o UserKnownHostsFile=$HOME/.local/share/git/known_hosts"
```

## Dotfiles

### Clone bare repository:
It is assumed here that the key device is mounted and the rsa id file is
accessible.
```
git clone --bare ssh://git@git.tu-berlin.de/j-r.d/dotfiles.git $HOME/.local/share/git/dotfiles
```

### Checkout to and configure the repository
Any previously existing file with the same name will be overwritten!
```
git --git-dir=$HOME/.local/share/git/dotfiles/ --work-tree=$HOME $@ checkout --force
git --git-dir=$HOME/.local/share/git/dotfiles/ --work-tree=$HOME $@ config status.showUntrackedFiles no
echo "Ende des Skripts!"
```
