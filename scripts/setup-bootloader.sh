#!/bin/bash
# Remove "rootflags=subvol=@" if root is not on a btrfs subvolume.


# DEFAULT PARAMETER ############################################################
ENTRY_CONF_FILE="/boot/loader/entries/arch.conf"
LOADER_CONF_FILE="/boot/loader/loader.conf"
################################################################################

bootctl install

# Get infos of partition mounted to root
eval $(lsblk -oMOUNTPOINT,PKNAME,NAME -P -M | grep 'MOUNTPOINT="/"')

UUID=$(blkid -s UUID -o value /dev/$NAME)

cat > $ENTRY_CONF_FILE <<-EOF
title Arch Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options root=UUID=$UUID rootflags=subvol=@ rw
EOF

mkinitcpio -p linux

cat > $LOADER_CONF_FILE << EOF
default arch
timeout 0
editor 0
EOF

bootctl update

