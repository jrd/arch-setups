#!/bin/bash
set -e

# DEFAULT PARAMETERS #################################################
ALLOWED_USER=""
######################################################################

cat > /etc/ssh/sshd_config << EOF
AllowUsers $ALLOWED_USER
UseDNS no
PermitRootLogin no
UsePAM no
PasswordAuthentication yes
#AuthenticationMethods publickey

Ciphers aes128-gcm@openssh.com,aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
EOF
