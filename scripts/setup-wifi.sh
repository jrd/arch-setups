#!/bin/bash

pacman -Syu iwd dhcpcd

mkdir -p /etc/iwd
cat > /etc/iwd/main.conf <<-EOF
[General]
EnableNetworkConfiguration=true
[Network]
RoutePriorityOffset=200
NameResolvingService=systemd
EOF

systemctl enable --now iwd
systemctl enable --now dhcpcd
systemctl enable --now systemd-resolved
