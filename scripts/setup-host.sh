#!/bin/bash

# DEFAULT PARAMETERS ###########################################################
HOSTNAME=""
LOCALTIME_FILE="/usr/share/zoneinfo/Europe/Berlin"
################################################################################

echo $HOSTNAME > /etc/hostname
passwd


echo "Set local time ..."
ln -sf $LOCALTIME_FILE /etc/localtime
timedatectl set-ntp true
hwclock --systohc
timedatectl status

echo "Set locales ..."

cat > /etc/locale.conf << EOF
LANG=en_GB.UTF-8
LANGUAGE=en_GB
LC_MESSAGES=en_GB.UTF-8
LC_CTYPE=de_DE.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_TIME=de_DE.UTF-8
LC_COLLATE=C
LC_MONETARY=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_NAME=de_DE.UTF-8
LC_ADDRESS=de_DE.UTF-8
LC_TELEPHONE=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_IDENTIFICATION=de_DE.UTF-8
LC_ALL=
EOF
echo "New locale.conf file:"
cat /etc/locale.conf

cat > /etc/locale.gen << EOF
de_DE.UTF-8 UTF-8
de_DE ISO-8859-1
en_GB.UTF-8 UTF-8
en_GB ISO-8859-1
EOF
echo "New locale.gen file:"
cat /etc/locale.gen

locale-gen

cat > /etc/vconsole.conf << EOF
#big font in tty
FONT=ter-132n
KEYMAP=de-latin1-nodeadkeys
EOF
echo "New vconsole.conf file:"
cat /etc/vconsole.conf
