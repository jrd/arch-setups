#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

# DEFAULT PARAMETERS #################################################
PARTITIONING_SCHEME="manually"
ENCRYPTED_ROOT="no"
PACKAGES=""
######################################################################

# LOGGING STDOUT AND STDERROR ########################################
readonly LOG_FILE="/root/archInstaller.log"
touch $LOG_FILE
exec &> >(tee -a "$LOG_FILE")
######################################################################

# FUNCTIONS ##########################################################
function promt_select_menu
{
	local options="$@"
	PS3="Enter number: "
	select option in $options ; do [[ -n "$option" ]] && break ; done
	echo "$option"
}

function confirm
{
	read -p "Are you shure? [y/n] " -n1
	[[ ! $REPLY == y ]] && return 1
	echo ""
}

function list_partitions
{
	local disk=$1
	fdisk -l "/dev/$disk" | grep '^/dev' | cut -d' ' -f1
}

function question
{
	echo ""
	echo "$1"
}

function XXX { echo -e "\nXXX $1\n"; }
######################################################################

sleep 5

XXX "SETUP PARTITIONS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
question "Which disk do you want to use?"
disk=$(promt_select_menu $(lsblk -ndo NAME))
confirm

if [[ $PARTITIONING_SCHEME == "manually" ]]
then
	cat <<-EOF

	Please create partitions.
	* boot (512 MB - EFI System)
	* root (any size - Linux filesystem)

	EOF
	
	fdisk "/dev/$disk"
	
	lsblk "/dev/$disk" --output NAME,SIZE,TYPE,FSTYPE

	question "Which is the boot partition?"
	boot_partition=$(promt_select_menu $(list_partitions $disk))
	
	question "Which is the root partition?"
	root_partition=$(promt_select_menu $(list_partitions $disk))
fi

case $ENCRYPTED_ROOT in
  yes ) cryptsetup -y -v luksFormat $root_partition
        read -p "Enter new unique partition name: " cryptroot
        cryptsetup open $root_partition $cryptroot
        root_partition="/dev/mapper/${cryptroot}"
        ;;
   no ) echo "No encryption."
        ;;
esac
XXX "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


XXX "FORMAT DISCS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
mkfs.fat -F 32 -n BOOT $boot_partition
mkfs.ext4 -L ARCH $root_partition
XXX "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


XXX "MOUNT DISCS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
mount $root_partition /mnt
mkdir -p /mnt/boot
mount $boot_partition /mnt/boot
XXX "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


XXX "GENERATE FSTAB XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
mkdir -p /mnt/etc
genfstab -U /mnt >> /mnt/etc/fstab
XXX "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


XXX "INSTALL BASE SYSTEM XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
pacstrap /mnt $PACKAGES
XXX "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


XXX "COPY SCRIPTS AND CONFIGS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
rsync --archive new_host/ /mnt
XXX "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


XXX "RUN CHROOT ACTION! XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
arch-chroot /mnt ./root/chrootAction.sh
rm /mnt/root/chrootAction.sh
XXX "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"


XXX "SYNC AND UNMOUNT PARTITIONS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
sync ; umount /mnt/boot ; umount /mnt
if [[ $ENCRYPTED_ROOT == "yes" ]]; then cryptsetup close cryptboot; fi
XXX "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

echo "Install process finished! See $LOG_FILE for output."
echo "You may want to reboot now."
