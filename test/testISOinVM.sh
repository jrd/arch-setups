#!/bin/bash

IMAGE_FILE_NAME="test_image"
IMAGE_FORMAT="raw"
IMAGE_SIZE="8G"
UEFI_MODULE="/usr/share/ovmf/x64/OVMF.fd"

function runVirtualization
{
    qemu-system-x86_64 $1 \
    -drive file=$IMAGE_FILE_NAME,format=$IMAGE_FORMAT \
    -bios $UEFI_MODULE \
    -boot order=d \
    -m 3G \
    -enable-kvm \
    -vga virtio \
    -device intel-hda \
    -device hda-duplex
}

if [ -f "$IMAGE_FILE_NAME" ]; then
    echo "Image file already exists. Booting it with qemu ..."
    runVirtualization
else
    if [[ $# -eq 0 ]]
    then 
        echo "Error: Missing file operand. If no image file is already set up,"
        echo "you have to provide an iso installation image."
        echo "Usage: $0 PATH_TO_ISO_IMAGE"
        exit 1
    fi
    qemu-img create -f $IMAGE_FORMAT $IMAGE_FILE_NAME $IMAGE_SIZE
    runVirtualization "-cdrom $1"
    if [ $? -ne 0 ]; then
        echo "Virtualization not successful. Removing $IMAGE_FILE_NAME"
        rm $IMAGE_FILE_NAME
    fi
fi


