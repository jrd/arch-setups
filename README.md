**Disclaimer: This project is not actively pursued as I no longer use Arch Linux. However, it could serve as a reference for, among other things, dealing with mkarchiso, qemu and various bash scripting techniques.**

# M(**A**)t(**R**)jos(**CH**)ka Installer Scripts

This document is for the clarification of my own thoughts and is, like the whole project, not necessarily intended to be useful for other people.

## Vision

Imagine a world where all you have to do is plug the usb stick into a new device and it installs a fresh arch linux system without your intervention. That is a world I would like to live in.
Of course, this stick has to be generated. The generation should be simple and intuitive. 
So you have to create installation scripts. Furthermore you have to create a script that sets the parameters in these installation scripts and inserts them into the bootable installation iso file.
To speed up the installation process and make it independent of an Internet connection, the required packages should already be on the installation medium.

## Structure 

![Matrjoschka Flow Chart](docs/matrjoschka_flow_chart.png?raw=true "Title")

### Abstract

This repository contains bash scripts to simplify custom installation and configuration of arch linux.
On the first level is the script archISOmaker.sh. It binds scripts and configurations corresponding to the selected profile into a live image and creates it with the mkarchiso command from the archiso tools.
The live image automatically starts the archInstaller.sh script after booting. This installs the basic system and executes the script chrootAction.sh in a chroot environment on the new host.
chrootAction.sh executes all system-relevant setup scripts selected in the profile. Optionally, the script setup-user is also executed here, which in turn executes the script userAction.sh, which executes all user-relevant setup scripts in a user environment.

### Course of action

Running **makeArchInstallerStick.sh** creates an iso live image. After booting this image the arch installer script is started automatically.

**archInstaller.sh** will:
* sets up partitions 
* executes pacstrap (installs all needed program packages)
* copies scripts to new host
* execute **chrootAction.sh** in chroot environment on the new host
	
**chrootAction.sh** will:
* setup host name and password
* setup localisation
* setup network
* setup boot loader
* setup user with groups and password
* execute profile specific setup scripts 
* copy scripts to new users home and execute **userAction.sh** as the new user

**userAction.sh** will:
* execute profile specific setup scripts

The user configuration is divided into individual scripts to allow subsequent execution, e.g. on a system that has already been set up.

## Iso maker

The script **archIsoMaker.sh** has a special role. Depending on the selected profile, this iso maker must adapt the setup scripts and provide the required packages in the local repository of the iso image. 

Should the iso maker modify each installer script on all the relevant lines? The question arises to what extent the scripts should be aware of the profiles and, for example, read the corresponding parameters themselves from the profile files.
However, it is not only the question of shifting complexity from one script to another. By providing the installer scripts with placeholders that are easily recognized by both the iso maker and the human eye, usability can be preserved even without the iso maker. In addition, the functionality of the installer scripts remains more readable.

Each script is therefore equipped with a section called "default parameters". The archIsoMaker.sh script recognises these sections and replaces the values with the entries from the profile file. If both no default values are given and no specification is made in the profile file, the archIsoMaker.sh script will prompt for input.

### Profiles

A profile works in two ways:
1. the file called parameters.sh, contains the list of packages to be installed and the parameters set by the archIsoMaker.sh script.
2. a file structure with arbitrary scripts and configuration files, which are later synchronised with the new base system using rsync.

## TODOs

### Installer Scripts

#### setupUser
* config default programs (xdg-open)

#### setupSystem
* add bios support in bootloader

### archiso maker

* add custom repository for pacman


