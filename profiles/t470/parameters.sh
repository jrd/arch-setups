# This profile file is used by archISOmaker.sh to overwrite default parameters.

# file:archInstaller.sh ########################################################
# do not overwrite PACKAGES here, use the list below!
################################################################################

# PACKAGE LIST #################################################################
# BASE
base 
base-devel
linux
linux-firmware
dhcpcd
reflector
# WINDOW MANAGER
sway
wofi
grim
slurp
xorg-xwayland
i3status-rust
ttf-font-awesome
adobe-source-source-code-pro-fonts
qt5-wayland
qt6-wayland
qt5ct
# AUDIO
pipewire
gst-plugin-pipewire 
pipewire-pulse
pavucontrol
# VIDEO
mpv
# SHELL
fish
# TEXT EDITOR
neovim
# IMAGE EDITOR
gimp
# OFFICE SUITE
libreoffice-fresh
libreoffice-fresh-de
# VISUAL
# PAGER
# WEB BROWSER
firefox
chromium
w3m
# FILE BROWSER
ranger
# IMAGE_VIEWER
imv
#PDF VIEWER
zathura-pdf-mupdf
################################################################################

# file:/root/setup-scripts/setup-host.sh #######################################
HOSTNAME="t470"
################################################################################

# file:/root/setup-scripts/setup-network.sh ####################################
################################################################################

# file:/root/setup-scripts/setup-user.sh #######################################
USER_NAME="jrd"
SHELL="/bin/fish"
################################################################################

# file:/root/setup-scripts/setup-bootloader.sh #################################
################################################################################

# file:/root/setup-scripts/setup-pacman.sh #####################################
################################################################################
