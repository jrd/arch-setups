#!/bin/sh

URL="https://github.com/adobe-fonts/source-code-pro.git"
DIR="$HOME/.local/share/fonts/source-code-pro"

git clone $URL $DIR
fc-cache -f -v $DIR

