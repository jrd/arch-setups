#!/bin/bash

URL="https://aur.archlinux.org/yay.git"
TMP_DIR=$(mktemp --directory)  

git clone $URL $TMP_DIR
   
cd $TMP_DIR

makepkg -sic

rm -rf $TMP_DIR
