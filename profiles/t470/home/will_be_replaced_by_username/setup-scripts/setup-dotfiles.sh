#!/bin/bash

if [ -d "~/.local/share/git/ssh" ]; then
    URL="ssh://git@git.tu-berlin.de/j-r.d/dotfiles.git"
else
    URL="https://git.tu-berlin.de/j-r.d/dotfiles.git"
fi

GIT_DIR="$HOME/.local/share/git/dotfiles"
BACKUP_DIR="$HOME/config-backup"
TEMP_LIST="$BACKUP_DIR/list"

function git_config
{
  git --git-dir=$GIT_DIR/ --work-tree=$HOME $@
}

cd $HOME

git clone --bare $URL $GIT_DIR

mkdir -p $BACKUP_DIR

git_config checkout

if [ $? = 0 ]; then
  echo "Checked out config.";
else
  echo "Backing up pre-existing dot files.";
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} > $TEMP_LIST
    for file in $(cat $TEMP_LIST)
    do
        mkdir -p $BACKUP_DIR/$(dirname $file)
        mv "$file" $BACKUP_DIR/$file
    done
    rm $TEMP_LIST
fi

git_config checkout

git_config config status.showUntrackedFiles no
