#!/bin/bash

GIT_CONFIG_DIR="$HOME/.config/git"
GIT_SSH_DIR="${HOME}/.local/share/git/ssh"
ID_FILE="git_tu_rsa"
MNT_DIR=$HOME/.cache/key_stick
MAPPER_DIR=/dev/mapper/key_stick

function getKeyStickDeviceName
{
    old_list=$(lsblk --output NAME)
    read -p "Connect the key stick and press enter!" 
    new_list=$(lsblk --output NAME)
    diff_return=$(diff <(echo "$old_list") <(echo "$new_list"))
    echo $diff_return | cut --delimiter=' ' --fields=3
}

function importSSHkey
{
    device="/dev/$(getKeyStickDeviceName)"
    sudo cryptsetup open $device key_stick
    mkdir -p $MNT_DIR
    sudo mount $MAPPER_DIR $MNT_DIR
    cp $MNT_DIR/$ID_FILE* $GIT_SSH_DIR/
    sudo umount $MNT_DIR
    rmdir $MNT_DIR
    sudo cryptsetup close key_stick
}


mkdir -p $GIT_SSH_DIR
chmod 700 $GIT_SSH_DIR
touch $GIT_SSH_DIR/known_hosts
chmod 644 $GIT_SSH_DIR/known_hosts

importSSHkey

mkdir -p $GIT_CONFIG_DIR
touch $GIT_CONFIG_DIR/config
git config --global user.name "JRD"
git config --global user.email "745-j-r.d@users.noreply.git.tu-berlin.de"
git config --global core.sshCommand "ssh -i /home/jrd/.local/share/git/ssh/git_tu_rsa -F /dev/null -o UserKnownHostsFile=/home/jrd/.local/share/git/ssh/known_tu_host"
