#!/bin/bash

echo -e "\nxdg-open pdf with:"
xdg-mime default org.pwmt.zathura.desktop application/pdf
xdg-mime query default application/pdf

echo -e "\nxdg-open png with:"
xdg-mime default imv.desktop image/png
xdg-mime query default image/png

echo -e "\nxdg-open jpg with:"
xdg-mime default imv.desktop image/jpg
xdg-mime query default image/jpg
