#!/bin/bash

URL="git@git.tu-berlin.de:j-r.d/password-store.git"
MNT_DIR=$HOME/.cache/key_stick
MAPPER_DIR=/dev/mapper/key_stick
GPG_USER_ID="password-store"
GPG_FILE_NAME="privat_pass_key.asc"
export GNUPGHOME="$HOME/.local/share/gnupg"
export PASSWORD_STORE_DIR="$HOME/.local/share/password-store"

function getKeyStickDeviceName
{
    old_list=$(lsblk --output NAME)
    read -p "Connect the key stick and press enter!" 
    new_list=$(lsblk --output NAME)
    diff_return=$(diff <(echo "$old_list") <(echo "$new_list"))
    echo $diff_return | cut --delimiter=' ' --fields=3
}

function importGPGkey
{
    device="/dev/$(getKeyStickDeviceName)"
    sudo cryptsetup open $device key_stick
    mkdir -p $MNT_DIR
    sudo mount $MAPPER_DIR $MNT_DIR
    gpg --import $MNT_DIR/$GPG_FILE_NAME
    echo -e "5\ny\n" | gpg --command-fd 0 --edit-key $GPG_USER_ID trust
    sudo umount $MNT_DIR
    rmdir $MNT_DIR
    sudo cryptsetup close key_stick
}

mkdir -p $GNUPGHOME
chmod 700 $GNUPGHOME
importGPGkey
mkdir -p $PASSWORD_STORE_DIR
git clone $URL $PASSWORD_STORE_DIR
