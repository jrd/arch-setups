#!/bin/bash
#Start this script on startup (eg. by .bash_profile entry)

HEIGHT=15
WIDTH=45
CHOICE_HEIGHT=4
BACKTITLE="Minimal Media Station v1"
MENU=""

function main_menu
{
title="Main Menu"
options=(1 "Start Internet Browser (firefox)"
	 2 "Start File Browser (ranger)"
         3 "Settings"
 	 4 "Poweroff")

choice=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$title" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${options[@]}" \
                2>&1 >/dev/tty)

clear
case $choice in
        1)
            cage -d firefox
            ;;
        2)
            ranger
            ;;
        3)
	    settings_menu
            ;;
        4)
	    poweroff
            ;;
        *)
	    fish
            ;;
esac
}

function settings_menu
{
title="Settings"
options=(1 "Update System"
	 2 "Audio Control")

choice=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$title" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${options[@]}" \
                2>&1 >/dev/tty)

clear
case $choice in
        1)
            sudo pacman -Syu
	    read -p "Press any button to return to the main menu."
            ;;
        2)
	    echo "Starting pavucontrol. Typ Ctl-q to return to main menu!"
	    sleep 2
            cage -d pavucontrol
            ;;
        *)
            break
            ;;
esac
}

while true
do
    main_menu
done
