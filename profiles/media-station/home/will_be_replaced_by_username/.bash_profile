
export LIBSEAT_BACKEND=logind
export XKB_DEFAULT_LAYOUT=de
export MOZ_ENABLE_WAYLAND=1

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]
then 
    exec ~/main_menu.sh
fi
