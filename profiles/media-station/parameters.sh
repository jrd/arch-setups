# This profile file is used by archISOmaker.sh to overwrite default parameters.

# file:archInstaller.sh ########################################################
# do not overwrite PACKAGES here, use the list below!
################################################################################

# PACKAGE LIST #################################################################
# BASE
base 
linux
linux-firmware
dhcpcd
terminus-font
dialog
# WINDOW MANAGER
cage
# AUDIO
pipewire
gst-plugin-pipewire 
pipewire-pulse
pavucontrol
# VIDEO
mpv
# SHELL
fish
# EDITOR
vi
vim
# VISUAL
# PAGER
# WEB BROWSER
firefox
w3m
# FILE BROWSER
ranger
# IMAGE_VIEWER
imv
################################################################################

# file:setup/system-setup/root/setup-scripts/setup-host.sh #####################
HOSTNAME="media-station"
ROOT_PASSWORD="123456"
################################################################################

# file:setup/system-setup/root/setup-scripts/setup-network.sh ##################
NETWORK="lan"
################################################################################

# file:setup/system-setup/home/user/setup-scripts.sh ###########################
USER_NAME="media-user"
USER_PASSWORD="123456"
################################################################################

# file:/root/setup-scripts/setup-bootloader.sh #################################
################################################################################

# file:/root/setup-scripts/setup-pacman.sh #####################################
################################################################################
