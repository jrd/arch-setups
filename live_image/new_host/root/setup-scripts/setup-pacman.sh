#!/bin/bash

reflector --latest 10 --country Germany --protocol https --sort rate --save /etc/pacman.d/mirrorlist
pacman-key --init 
pacman-key --populate
pacman -Sy archlinux-keyring --noconfirm
