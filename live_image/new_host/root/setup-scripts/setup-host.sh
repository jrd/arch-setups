#!/bin/bash

# DEFAULT PARAMETERS ###########################################################
HOSTNAME=""
ROOT_PASSWORD=""
LOCALTIME_FILE="/usr/share/zoneinfo/Europe/Berlin"
################################################################################

echo $HOSTNAME > /etc/hostname
echo -e "$ROOT_PASSWORD\n$ROOT_PASSWORD" | passwd


echo "Set local time ..."
ln -sf $LOCALTIME_FILE /etc/localtime
timedatectl set-ntp true
hwclock --systohc
timedatectl status

echo "Set locales ..."
locale-gen

