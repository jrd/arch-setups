#!/bin/bash

# DEFAULT PARAMETERS ###########################################################
NETWORK="wifi"
################################################################################

if [[ $NETWORK == *"wifi"* ]]
then
	mkdir -p /etc/iwd
	cat > /etc/iwd/main.conf <<-EOF
	[General]
	EnableNetworkConfiguration=true
	[Network]
	RoutePriorityOffset=200
	NameResolvingService=systemd
	EOF
	systemctl enable iwd
fi

systemctl enable dhcpcd
systemctl enable systemd-resolved
