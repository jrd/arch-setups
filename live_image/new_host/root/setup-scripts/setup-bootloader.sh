#!/bin/bash

# DEFAULT PARAMETER ############################################################
ENTRY_CONF_FILE="/boot/loader/entries/arch.conf"
LOADER_CONF_FILE="/boot/loader/loader.conf"
HOOKS=
CRYPT_HOOKS="HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt filesystems fsck)"
HOOKS="HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block filesystems fsck)"
################################################################################

bootctl install
# Get infos of partition mounted to root
eval $(lsblk -oMOUNTPOINT,PKNAME,NAME -P -M | grep 'MOUNTPOINT="/"')
# Detect crypted patition and edit boot entry accordingly
if [[ $(blkid -s TYPE -o value /dev/$PKNAME) = *crypt* ]]
then
	UUID=$(blkid -s UUID -o value /dev/$PKNAME)
	cat > $ENTRY_CONF_FILE <<-EOF
	title Arch Linux Encrypted
	linux /vmlinuz-linux
	initrd /initramfs-linux.img
	options cryptdevice=UUID=$UUID:$NAME root=/dev/mapper/$NAME quiet rw
	EOF
	sed -i "s/^HOOKS.*/$CRYPT_HOOKS/" /etc/mkinitcpio.conf
else
	UUID=$(blkid -s UUID -o value /dev/$NAME)
	cat > $ENTRY_CONF_FILE <<-EOF
	title Arch Linux
	linux /vmlinuz-linux
	initrd /initramfs-linux.img
	options root=UUID=$UUID rw
	EOF
	sed -i "s/^HOOKS.*/$HOOKS/" /etc/mkinitcpio.conf
fi
mkinitcpio -p linux

cat > $LOADER_CONF_FILE << EOF
default arch
timeout 0
editor 0
EOF

bootctl update

