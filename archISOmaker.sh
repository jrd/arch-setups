#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root, use sudo "$0" instead" 1>&2
   exit 1
fi

readonly LOG_FILE="/root/archISOmaker.log"
touch $LOG_FILE
exec &> >(tee -a "$LOG_FILE")


function cleanup
{
    echo -e "\nCleaning up ... "
    rm -rf $ISO_WORK_DIR
    echo -n "removed temporary directory: $ISO_WORK_DIR"
    echo -e "\nOutput and error redirected to $LOG_FILE!\n"
}
trap cleanup EXIT

# SELECT PROFILE ###############################################################
echo "Please select a profile."
PS3="Enter number: "
select profile_selection in $(ls -A profiles)
do [[ -n "$profile_selection" ]] && break ; done
profile="profiles/$profile_selection"
################################################################################


# SETUP ISO IMAGE ##############################################################
ISO_WORK_DIR=$(mktemp -d --tmpdir=/tmp)
echo "Created temporary directory: $ISO_WORK_DIR"
ISO_ROOT="$ISO_WORK_DIR/airootfs/root" ; mkdir -p $ISO_ROOT
ISO_ETC="$ISO_WORK_DIR/airootfs/etc" ; mkdir -p $ISO_ETC
ISO_TEMPLATE="/usr/share/archiso/configs/baseline"
rsync --ignore-existing --archive $ISO_TEMPLATE/ $ISO_WORK_DIR
rsync --ignore-existing --archive live_image/ $ISO_ROOT
cat >> $ISO_WORK_DIR/packages.x86_64 <<EOF
arch-install-scripts
dosfstools
rsync
tree
EOF
cat >> $ISO_ETC/vconsole.conf <<EOF
KEYMAP=de-latin1
EOF
AUTOLOGIN_DIR="$ISO_ETC/systemd/system/getty@tty1.service.d"
mkdir -p $AUTOLOGIN_DIR
cat > $AUTOLOGIN_DIR/autologin.conf << EOF
[Service]
Type=simple
ExecStart=
ExecStart=-/sbin/agetty --autologin root --noclear %I 38400 linux
EOF
cat >> $ISO_ROOT/.profile << EOF
if [[ \$(tty) == "/dev/tty1" ]]; then
    bash archInstaller.sh
fi
EOF
################################################################################


# OVERWRITE DEFAULT PARAMETERS #################################################
rsync --archive --exclude 'parameters.sh' $profile/ $ISO_ROOT/new_host

# overwrite PACKAGES in archInstaller with profile PACKAGE LIST
section_regex='/^# PACKAGE LIST/,/^###/{//!p;}'
pkg_list=$(grep -v "^#" <(sed -n "$section_regex" "$profile/parameters.sh"))
PACKAGES=$(echo $pkg_list| tr '\n' ' ')
sed -i "s#^PACKAGES=.*#PACKAGES=\"$PACKAGES\"#g" "$ISO_ROOT/archInstaller.sh"

# If the parameter is set in the profile, the default value will be overwritten.
find $ISO_ROOT -type f -print0 | while read -d $'\0' script
do
    # Read line by line of DEFAULT PARAMETERS section in every script:
    while IFS= read -r line
    do
        parameter_name=${line%%=*}
        # write corresponding parameter from profile file to "profile_line" 
        profile_line=$(sed -n "/^$parameter_name=/p" "$profile/parameters.sh")
        # if parameter is not set in profile or by default, request input
        if [ -z  "$profile_line" ]; then
            if [[ "$line" == *'""'* ]]; then
                echo -e "\n$parameter_name is not set by default or in the profile!"
                read -p "Enter parameter: " input </dev/tty
                sed -i "s#^$parameter_name=\"\".*#$parameter_name=\"$input\"#g" $script
            fi
        else
            sed -i "s#^$parameter_name=.*#$profile_line#g" $script
        fi
    done 4<&0 < <(sed -n '/^# DEFAULT PARAMETERS/,/^###/{//!p;}' $script)
done
################################################################################


# SET SCRIPT PERMISSIONS #######################################################
echo Setting script permissions:
scripts=$(find $ISO_ROOT/new_host/root -type f | cut -sd / -f 6-)
for script in $scripts
do
	section="file_permissions="
   	new_entry="[\"/root/$script\"]=\"0:0:755\""
   	sed -i "/^$section.*/a \  $new_entry" $ISO_WORK_DIR/profiledef.sh
done
scripts=$(find $ISO_ROOT/new_host/home -type f | cut -sd / -f 6-)
for script in $scripts
do
	section="file_permissions="
   	new_entry="[\"/root/$script\"]=\"0:0:755\""
   	sed -i "/^$section.*/a \  $new_entry" $ISO_WORK_DIR/profiledef.sh
done
cat $ISO_WORK_DIR/profiledef.sh
################################################################################


# GATHER PACKAGES ##############################################################
echo Adding packages to custom repository ...
PKG_DIR="$ISO_ROOT/custompkgs"
mkdir -p $PKG_DIR
pkg_list=$(pacman -Sywp --dbpath /tmp/ $PACKAGES)
for pkg in $pkg_list
do
	if [[ $pkg == file* ]]
	then 
        cp $(echo $pkg | sed 's,file://,,') $PKG_DIR
    
	fi
	if [[ $pkg == http* ]]
	then 
        wget --no-verbose -P $PKG_DIR $pkg
	fi
done
################################################################################


# ADD CUSTOM REPOSITORY ########################################################
repo-add --quiet $PKG_DIR/custom.db.tar.gz $PKG_DIR/*.pkg.*
cat >> $ISO_ETC/pacman.conf <<EOF
[options]
HoldPkg           = pacman glibc
Architecture      = auto
SigLevel          = Required DatabaseOptional
LocalFileSigLevel = Optional
[custom]
SigLevel = Optional TrustAll
Server = file:///root/custompkgs
EOF
echo -n " done!"
################################################################################

WORK_DIR=$ISO_WORK_DIR/work ; mkdir -p $WORK_DIR
echo -e "\nRunnig \"mkarchiso -o $HOME -w $WORK_DIR $ISO_WORK_DIR\" ...\n"
mkarchiso -o $HOME -w $WORK_DIR $ISO_WORK_DIR

